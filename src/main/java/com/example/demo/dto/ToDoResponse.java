package com.example.demo.dto;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

public class ToDoResponse {
	@NotNull
	public Long id;

	@NotNull
	public String text;

	public ZonedDateTime completedAt;
}
