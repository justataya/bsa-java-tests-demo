package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ToDoAllLayersIT {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenUpsertNoId_thenReturnNew() throws Exception {
        //call
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        ToDoSaveRequest todo = new ToDoSaveRequest();
        todo.text = "Test todo 1";

        //call
        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON).content(ow.writeValueAsString(todo)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.text").value(todo.text))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetAll_thenThrowNotFoundException() throws Exception {
        //call
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        ToDoSaveRequest todo = new ToDoSaveRequest();
        todo.id = 10L;
        todo.text = "Test todo 1";

        this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON).content(ow.writeValueAsString(todo)))
                .andExpect(status().isNotFound());
    }
}
