package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        when(toDoRepository.findAll()).thenReturn(
                Collections.singletonList(
                        new ToDoEntity(1L, testText)
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenCompleted_thenReturnWithCompletedNotNull() throws Exception {
        //mock
        var testToDos = new ArrayList<ToDoEntity>();
        testToDos.add(new ToDoEntity(0L, "Test 1"));
        var toDo = new ToDoEntity(1L, "Test 2");
        toDo.completeNow();
        testToDos.add(toDo);
        when(toDoRepository.findAllByCompletedAtNotNull())
                .thenReturn(testToDos.stream().filter(item -> item.getCompletedAt() != null).collect(Collectors.toList()));

        //validate
        this.mockMvc
                .perform(get("/todos/completed"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(toDo.getText()))
                .andExpect(jsonPath("$[0].id").value(toDo.getId()))
                .andExpect(jsonPath("$[0].completedAt").value(toDo.getCompletedAt().format(DateTimeFormatter.ISO_DATE_TIME)));
    }

    @Test
    void whenGetCompleted_thenReturnCompletedAt() throws Exception {
        var startTime = ZonedDateTime.now(ZoneOffset.UTC);
        //mock
        var todo = new ToDoEntity(0L, "Test 1");
        todo.completeNow();

        when(toDoRepository.findAllByCompletedAt(startTime)).thenReturn(List.of(todo));

        //validate
        this.mockMvc
                .perform(get("/todos/completed").param("date",startTime.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(todo.getText()))
                .andExpect(jsonPath("$[0].id").value(todo.getId()))
                .andExpect(jsonPath("$[0].completedAt").value(todo.getCompletedAt().format(DateTimeFormatter.ISO_DATE_TIME)));
    }


    @Test
    void whenGetCompleted_thenThrowIllegalArgumentException() throws Exception {
        this.mockMvc
                .perform(get("/todos/completed").param("date", ZonedDateTime.now(ZoneOffset.UTC).plusDays(1).toString()))
                .andExpect(status().isBadRequest());
    }

}
